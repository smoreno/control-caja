import { Router } from 'express';
import { checkJwt } from '../../core/middlewares/check-jwt';
import { projectController } from './project.controller';

class ProjectRoutes {
  public router: Router = Router();

  constructor() {
    this.config();
  }

  public config(): void {
    this.router.get('/', checkJwt, projectController.ctrlList);
    this.router.get('/:id([0-9]+)', checkJwt, projectController.ctrlGetOne);
    this.router.post('/', checkJwt, projectController.ctrlCreate);
    this.router.patch('/', checkJwt, projectController.ctrlUpdate);
    this.router.delete('/:id([0-9]+)', checkJwt, projectController.ctrlRemove);
  }
}

export default new ProjectRoutes().router;
