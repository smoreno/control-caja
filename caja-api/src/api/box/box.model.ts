import { DeleteResult, getManager } from 'typeorm';
import { Box } from '../../core/database/entities/box';

export class BoxModel {
  public async list(): Promise<Box[]> {
    //  Get repository to operations
    const BoxRepository = getManager().getRepository(Box);

    // Return the list of bills
    return await BoxRepository.find({});
  }

  public async getOne(BoxId: number): Promise<Box> {
    //  Get repository to operations
    const BoxRepository = getManager().getRepository(Box);

    // Return Box by id
    return await BoxRepository.findOne({
      relations: [ 'user'],
      where: { id: BoxId},
    });
  }

  public async saveChanges(departament: Box): Promise<Box> {
    return await getManager().getRepository(Box).save(departament);
  }

  public async remove(BoxId: number): Promise<DeleteResult> {
    return await getManager().getRepository(Box).delete(BoxId);
  }
}
