import { IEnvironment } from '../../interfaces/i-environment';

export const production: IEnvironment = {
  ENVIRONMENT: 'production',
  NODE_PORT: '3000',
  NODE_SERVER: 'https://',
  CLIENT_SERVER: 'https://',
  DB_PORT: '3306',
  DB_HOST: 'us-cdbr-iron-east-05.cleardb.net',
  DB_NAME: 'system_box',
  DB_USER: 'root',
  DB_PASSWORD: 'root',
  DB_CHARSET: 'utf8_general_ci',
  DB_SYNCHRONIZE: true,
  DB_LOGGING: false,
};
