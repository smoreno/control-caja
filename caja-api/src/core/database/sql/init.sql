-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 21-11-2019 a las 20:14:27
-- Versión del servidor: 5.5.24-log
-- Versión de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `system_box`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `box`
--

CREATE TABLE IF NOT EXISTS `box` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `money` int(20) DEFAULT NULL,
  `observation` varchar(500) DEFAULT NULL,
  `day_open` date DEFAULT NULL,
  `day_close` date DEFAULT NULL,
  `id_departament` int(11) DEFAULT NULL,
  `id_supervisor` int(5) DEFAULT NULL,
  `id_state` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_departament` (`id_departament`),
  KEY `id_supervisor` (`id_supervisor`),
  KEY `id_state` (`id_state`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departament`
--

CREATE TABLE IF NOT EXISTS `departament` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `departament`
--

INSERT INTO `departament` (`id`, `name`) VALUES
(1, 'Tecnologia'),
(2, 'Administracion'),
(3, 'RRHH');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movement`
--

CREATE TABLE IF NOT EXISTS `movement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num_fact` int(11) DEFAULT NULL,
  `description` varchar(300) DEFAULT NULL,
  `day_fact` date DEFAULT NULL,
  `amount_fact` int(20) DEFAULT NULL,
  `attached` varchar(300) DEFAULT NULL,
  `residue` int(20) DEFAULT NULL,
  `id_box` int(11) DEFAULT NULL,
  `ci_responsable` int(11) DEFAULT NULL,
  `id_project` int(3) DEFAULT NULL,
  `id_expense` int(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_box` (`id_box`),
  KEY `ci_responsable` (`ci_responsable`),
  KEY `id_project` (`id_project`),
  KEY `id_expense` (`id_expense`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `location` varchar(300) NOT NULL,
  `description` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `project`
--

INSERT INTO `project` (`id`, `name`, `location`, `description`) VALUES
(1, 'CERTAINET CONSULTORES C.A.', 'Av. Libertador, CC Av. Libertador', 'Servicio tecnologico'),
(2, 'VINCCLER C.A', 'Av. La Salle, Edf. Fiorela', 'Sector Construccion'),
(3, 'COCOTHAI', 'Av. Ppal. de Las Mercedes, C.C. El Tolón', 'Restaurante'),
(4, 'CETRIOLO', 'Av. Francisco de Miranda, Empresarial Galipán', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `role`
--

INSERT INTO `role` (`id`, `name`) VALUES
(1, 'admin'),
(2, 'supervisor'),
(3, 'operador'),
(4, 'n/t');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `state`
--

CREATE TABLE IF NOT EXISTS `state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `state`
--

INSERT INTO `state` (`id`, `name`) VALUES
(1, 'activo'),
(2, 'por supervisar'),
(3, 'cerrado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `type_expense`
--

CREATE TABLE IF NOT EXISTS `type_expense` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `type_expense`
--

INSERT INTO `type_expense` (`id`, `name`, `description`) VALUES
(1, 'ESTADIA', 'Alojamiento, hospedaje'),
(2, 'CONSUMO', 'Comidas,desayuno,almuerzo,cena,refrigerio,bebidas no alcoholicas'),
(3, 'TRASLADO TERRESTRE', 'Tasa aeroportuaria, impuestos de salida, peajes'),
(4, 'TRASLADO AEREO', 'Tasa aeroportuaria, impuestos de salida, peajes'),
(5, 'TELECOMUNICACION', 'Gasto de telefono, tarjetas telefocinas, centros de comunicaciones, internet'),
(6, 'VEHICULO', 'Estacionamiento, consumible y lubricantes'),
(7, 'AUTOSERVICIO', 'Reparaciones y mantenimiento de vehiculos'),
(8, 'GASTOS GENERALES', 'Gastos generales no deducibles, multas y sanciones'),
(9, 'MATERIALES', 'Gastos de materiales, suministros y herramientas'),
(10, 'VARIOS', 'Gastos varios menores');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `ci` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `mail` varchar(80) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `id_role` int(11) DEFAULT NULL,
  `id_departament` int(11) DEFAULT NULL,
  PRIMARY KEY (`ci`),
  KEY `id_role` (`id_role`),
  KEY `id_departament` (`id_departament`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`ci`, `name`, `last_name`, `mail`, `password`, `id_role`, `id_departament`) VALUES
(1, 'admin', 'admin', 'admin', 'control.box', 1, 1),
(6003944, 'Vilma', 'Dalmars', NULL, NULL, NULL, 1),
(9612412, 'Ana', 'Carrillo', 'acarrillo@hardwell.com', 'hardwell', 2, 1),
(9962256, 'Jose Miguel', 'Herrera', 'jmherrera@hardwell.com', 'hardwell', 2, 1),
(13379109, 'Shamil', 'Moreno', NULL, NULL, 4, 1),
(17145012, 'Victor', 'Dos Santos', NULL, NULL, 4, 1),
(18837118, 'Richarson', 'Teran', NULL, NULL, 4, 1),
(26576824, 'David', 'Pineda', NULL, NULL, 4, 1),
(26683555, 'David', 'Oliveros', NULL, NULL, 4, 1),
(27513392, 'Carlos', 'Escobar', NULL, NULL, 4, 1),
(27598659, 'Yambraily', 'Martinez', 'yambraily.martinez@hardwell.com', 'hardwell', 3, 1),
(27784515, 'Aaron', 'Perez', NULL, NULL, 4, 1);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `box`
--
ALTER TABLE `box`
  ADD CONSTRAINT `box_ibfk_1` FOREIGN KEY (`id_departament`) REFERENCES `departament` (`id`),
  ADD CONSTRAINT `box_ibfk_2` FOREIGN KEY (`id_supervisor`) REFERENCES `user` (`ci`),
  ADD CONSTRAINT `box_ibfk_3` FOREIGN KEY (`id_state`) REFERENCES `state` (`id`);

--
-- Filtros para la tabla `movement`
--
ALTER TABLE `movement`
  ADD CONSTRAINT `movement_ibfk_1` FOREIGN KEY (`id_box`) REFERENCES `box` (`id`),
  ADD CONSTRAINT `movement_ibfk_2` FOREIGN KEY (`ci_responsable`) REFERENCES `user` (`ci`),
  ADD CONSTRAINT `movement_ibfk_3` FOREIGN KEY (`id_project`) REFERENCES `project` (`id`),
  ADD CONSTRAINT `movement_ibfk_4` FOREIGN KEY (`id_expense`) REFERENCES `type_expense` (`id`);

--
-- Filtros para la tabla `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`id_role`) REFERENCES `role` (`id`),
  ADD CONSTRAINT `user_ibfk_2` FOREIGN KEY (`id_departament`) REFERENCES `departament` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
