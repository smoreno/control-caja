import { Component, OnInit, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';
import { IResponse } from 'src/app/share/interfaces/iresponse';
import { ToastrService } from 'ngx-toastr';

// MODAL
import { NgbActiveModal, NgbModalRef, NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

//interfaces
import { Administrator } from 'src/app/share/interfaces/administrator';

//Services
import { AdministratorService } from 'src/app/core/services/administrator.service';
import { DepartamentService } from 'src/app/core/services/departament.service';



@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {

  public registerUserForm: FormGroup;
  public administrator: Administrator;
  public departaments: any;
  public projects: any;
  public isEdit= false;
  public submitted = false;


  /**
   * Class constructor
   * @param activeModal 
   * @param formBuilder 
   * @param toastrService 
   * @param departamentService 
   * @param administratorService 
   * @param router 
   */
  constructor(
    public activeModal: NgbActiveModal, // MODAL
    // private modalService: NgbModal,
    private formBuilder: FormBuilder,
    // private lsService: LocalStorageService,
    private toastrService: ToastrService,
    private departamentService: DepartamentService,
    private administratorService: AdministratorService,
    // private projectService: ProjectServsice,
    private router: Router) {}
    
    close() {
      this.activeModal.dismiss();
    }

    
    ngOnInit() {

      // Build form
      this.registerUserForm = this.formBuilder.group({
      documentNumber: ['', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(15),
      ]],
      firstName: ['', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(15),
      ]],
      lastName: ['', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(15),
      ]],
      role: ['', [
       Validators.required,
      ]],
      email: ['', [
        Validators.required,
        Validators.pattern(/\b\w+([\.\+\-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+\b/)
      ]],
      password: ['', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(10),
      ]],
      departament: ['', [
        Validators.required,
      ]],
    });
    this.onSubmit();
    this.loadCombo();
  }

  get f() { return this.registerUserForm.controls; }

  
  loadCombo() {
    // Get all countries
    this.departamentService.listdepartaments().subscribe(
      (data: IResponse) => this.departaments = data.response,
      err => console.log(err)
    );
  }

  /**
   * Load the data in the user of the update
   * @param id ID of the sent user
   */
  onSubmit(id?: number): void {
    // I clean the form
    this.registerUserForm.reset();
    this.submitted = false;

    // I create an initial object
    this.administrator = {} as Administrator;

    // I verify the property `id`
    if (id) {
      // this.title = 'Editar user';
      // Get of user
      this.administratorService.byId(id).subscribe(
        (data: IResponse) => {
          this.registerUserForm.setValue({
            documentNumber: data.response.user.documentNumber,
            firstName: data.response.user.firstName,
            lastName: data.response.user.lastName,
            role: data.response.user.role,
            email: data.response.user.email,
            password: data.response.user.password,
            departamentId: data.response.user.departamentId,          
          });

          this.isEdit = true;
        },
          err => console.log(err)
        );
    } else {
      // this.title = 'Nuevo user';
      this.isEdit = false;
    }    
  }

 /**
   * Form Validation
   */
  save() {
    this.submitted = true;
    if (this.registerUserForm.invalid) {
      return;
    }

    // Verifico el método a utilizar
    const method = (this.administrator.id) ? 'update' : 'create';

    // Cargo los datos del objeto a enviar
    this.administrator.documentNumber = this.registerUserForm.get('documentNumber').value;
    this.administrator.firstName = this.registerUserForm.get('firstName').value;
    this.administrator.lastName = this.registerUserForm.get('lastName').value;
    this.administrator.role = this.registerUserForm.get('role').value;
    this.administrator.email = this.registerUserForm.get('email').value;
    this.administrator.password = this.registerUserForm.get('password').value;
    this.administrator.departamentId = this.registerUserForm.get('departament').value;

    this.administratorService[method](this.administrator).subscribe(
        (data: IResponse) => {
        if (data.code === 200) {
          this.toastrService.success('Usuario creado con éxito');
          this.activeModal.dismiss();
        } else {
          this.toastrService.error(data.response.Message);
        }
      },
      err => console.log(err)
    );
    
  }
}
