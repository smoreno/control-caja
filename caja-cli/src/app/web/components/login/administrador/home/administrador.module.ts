import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//MODULOS
import { AdministradorRoutingModule } from './administrador-routing.module';
import { SharedModule } from 'src/app/share/share.module';
import { FormsModule } from '@angular/forms'

//COMPONENTS
import { AdministradorComponent } from './administrador.component';
import { UsuariosComponent } from '../usuarios/user-list/usuarios.component';
import { BusinessListComponent } from '../empresas/business-list/business-list.component';
import { BusinessDetailComponent } from '../empresas/business-detail/business-detail.component';
import { DepartamentosComponent } from '../departamentos/departamentos.component';
import { UserDetailComponent } from '../usuarios/user-detail/user-detail.component';
import { DetaildepComponent } from '../departamentos/detaildep/detaildep.component';




@NgModule({
  imports: [
    CommonModule,
    AdministradorRoutingModule,
    SharedModule,
    FormsModule,
  ],
  declarations: [
    AdministradorComponent,
    UsuariosComponent,
    BusinessListComponent,
    BusinessDetailComponent,
    DepartamentosComponent,
    UserDetailComponent,
    DetaildepComponent,
  ],
})
export class AdministradorModule { }
