import { Component, OnInit, TemplateRef } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

//COMPONENT
import { CreateBoxComponent } from '../create-box/create-box.component';
import { MovCajaComponent } from '../mov-caja/mov-caja.component';

//MODAL
import {NgbModal } from '@ng-bootstrap/ng-bootstrap';

// INTERFACES
import { IResponse } from 'src/app/share/interfaces/iresponse';
import { Box } from 'src/app/share/interfaces/box';

//SERVICES
import { BoxService } from 'src/app/core/services/box.service';
import { ToastrService } from 'ngx-toastr';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';
import { DateTableDetailComponent } from '../date-table-detail/date-table-detail.component';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements OnInit {
  closeResult: string;
  public box: Box;
  public data: any;
  public validar = true;

/**
 * 
 * @param modalService 
 * @param formBuilder 
 * @param toastrService 
 * @param boxService 
 */
  constructor(
    private modalService: NgbModal,
    private toastrService: ToastrService,
    private boxService: BoxService,
    private lsService: LocalStorageService,

  ) { }

  
  ngOnInit() {
    this.loadTabla();
    if(this.lsService.getValue('role') === 'Supervisor'){
      this.validar = false;
    }
    
  }

  open() {
    this.modalService.open(CreateBoxComponent);
 }
 
 openMov() {
   this.modalService.open(DateTableDetailComponent);
}
  loadTabla() {
    // Get all countries
    this.boxService.listBoxs().subscribe(
      (data: IResponse) => {
        this.toastrService.success('Datos cargados con éxito');
        this.data = data.response;
        console.log(this.data);
      },
      err => console.log(err)

    );
  }
}
