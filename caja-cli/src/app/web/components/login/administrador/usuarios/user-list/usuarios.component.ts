import { Component, OnInit, ViewChild } from '@angular/core';

// MODAL
import {NgbModal } from '@ng-bootstrap/ng-bootstrap';

// COMPONENT
import { UserDetailComponent } from '../user-detail/user-detail.component';

// Interfaces
import { IResponse } from 'src/app/share/interfaces/iresponse';

// Services
import { AdministratorService } from 'src/app/core/services/administrator.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent implements OnInit {
  // Properties
  @ViewChild(UserDetailComponent) userDetailComponent: UserDetailComponent;

    closeResult: string;
    public administrator: any;
    public data: any;
    public usuarios: any;
    
    constructor(
      private modalService: NgbModal,
      private administratorService: AdministratorService,
      private toastrService: ToastrService,
      ) {
    }
    
      
  ngOnInit() {
    this.loadTabla();
    // this.filtrar();
  }


  open() {
     this.modalService.open(UserDetailComponent);
  }
  /**
   * Close the modal window
   */
  loadTabla() {
    // Get all countries
    this.administratorService.listAdministrators().subscribe(
      (data: IResponse) => {
        this.toastrService.success('Datos cargados con éxito');
        this.data = data.response;
      },
      err => console.log(err)

    );
  }

}
