import { IEnvironment } from '../../interfaces/i-environment';

export const local: IEnvironment = {
  ENVIRONMENT: 'local',
  NODE_PORT: '3000',
  NODE_SERVER: 'http://localhost:3000',
  CLIENT_SERVER: 'http://localhost:4200',
  DB_PORT: '3306',
  DB_HOST: 'localhost',
  DB_NAME: 'system_box',
  DB_USER: 'root',
  DB_PASSWORD: 'root',
  DB_CHARSET: 'utf8_general_ci',
  DB_SYNCHRONIZE: true,
  DB_LOGGING: false,
};
