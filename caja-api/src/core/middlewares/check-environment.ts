import dotenv from 'dotenv';
import { development } from '../configs/environments/development';
import { local } from '../configs/environments/local';
import { production } from '../configs/environments/production';
import { IEnvironment } from '../interfaces/i-environment';

export const checkEnvironment = () => {
  // Loading the dotenv configuration
  dotenv.config();

  // Getting the variables from environment
  let env: IEnvironment;
  switch (process.env.NODE_ENV) {
    case 'development': env = development; break;
    case 'production': env = production; break;
    default: env = local; break;
  }

  // Return vars
  return env;
};
