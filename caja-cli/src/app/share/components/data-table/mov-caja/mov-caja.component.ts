import { Component, OnInit } from '@angular/core';

// MODAL
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

//FORMS
import { FormGroup } from '@angular/forms';

// INTERFACES
import { Movement } from 'src/app/share/interfaces/movement';

@Component({
  selector: 'app-mov-caja',
  templateUrl: './mov-caja.component.html',
  styleUrls: ['./mov-caja.component.scss']
})
export class MovCajaComponent implements OnInit {

  public registerMovForm: FormGroup;
  public movement: Movement;

  constructor(
    public activeModal: NgbActiveModal,
  ) { }

  close() {
    this.activeModal.dismiss();
  }

  ngOnInit() {
  }

}
