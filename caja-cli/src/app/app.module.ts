import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { NgModule } from '@angular/core';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

// MODDULES
import { CoreModule } from './core/core.module';
import { SharedModule } from './share/share.module';

// COMPONENTES
import { AppComponent } from './app.component';
import { LoginComponent } from './web/components/login/login.component';
import { HeaderComponent } from './web/components/header/header.component';
import { NotFoundComponent } from './web/components/not-found/not-found.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    NotFoundComponent
  ],

  exports: [
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    ModalModule.forRoot(),
    CoreModule,
    SharedModule,
  ],
  providers: [],
  bootstrap: [AppComponent, HeaderComponent]
})
export class AppModule { }
