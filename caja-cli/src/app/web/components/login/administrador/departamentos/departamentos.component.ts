import { Component, OnInit } from '@angular/core';

//MODAL
import {NgbModal } from '@ng-bootstrap/ng-bootstrap';

//COMPONENT
import { DetaildepComponent } from './detaildep/detaildep.component';

// Interfaces
import { IResponse } from 'src/app/share/interfaces/iresponse';

// SERVICE
import { ToastrService } from 'ngx-toastr';
import { DepartamentService } from 'src/app/core/services/departament.service';

@Component({
  selector: 'app-departamentos',
  templateUrl: './departamentos.component.html',
  styleUrls: ['./departamentos.component.scss']
})
export class DepartamentosComponent implements OnInit {
  public data: any;

  constructor(
    private modalService: NgbModal,
    private departamentService: DepartamentService,
    private toastrService: ToastrService,
  ) { }

  ngOnInit() {
    this.loadTabla();
  }
  
  open() {
    this.modalService.open(DetaildepComponent);
  }

  loadTabla() {
    // Get all countries
    this.departamentService.listdepartaments().subscribe(
      (data: IResponse) => {
        this.toastrService.success('Datos cargados con éxito');
        this.data = data.response;
        console.log(this.data);
      },
      err => console.log(err)

    );
  }
}
