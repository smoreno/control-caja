import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';
import { ToastrService } from 'ngx-toastr';

// MODAL
import { NgbActiveModal, NgbModalRef, NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

// Interfaces
import { Departament } from 'src/app/share/interfaces/departament';
import { IResponse } from 'src/app/share/interfaces/iresponse';

// Services
import { DepartamentService } from 'src/app/core/services/departament.service';

@Component({
  selector: 'app-detaildep',
  templateUrl: './detaildep.component.html',
  styleUrls: ['./detaildep.component.scss']
})
export class DetaildepComponent implements OnInit {
  public registerDepForm: FormGroup;
  public departament: Departament;
  public isEdit= false;
  public submitted = false;
  
  constructor(
    public activeModal: NgbActiveModal, // MODAL
    private formBuilder: FormBuilder,
    // private lsService: LocalStorageService,
    private departamentService: DepartamentService,
    private toastrService: ToastrService,
    private router: Router) {}

    close() {
      this.activeModal.dismiss();
    }

  ngOnInit() {

    // Build form
    this.registerDepForm = this.formBuilder.group({
      name: ['', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(30),
      ]],
      floor: ['', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(5),
      ]],
      office: ['', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(5),
      ]],
    });
    
    this.onSubmit();
  }

  get f() { return this.registerDepForm.controls; }

 
 
/**
   * Load the data in the departament of the update
   * @param id ID of the sent departament
   */
  onSubmit(id?: number): void {
    console.log(this.registerDepForm);
    // I clean the form
    this.registerDepForm.reset();
    this.submitted = false;

    // I create an initial object
    this.departament = {} as Departament;

    // I verify the property `id`
    if (id) {
      // this.title = 'Editar departament';
      // Get of departament
      this.departamentService.byId(id).subscribe(
        (data: IResponse) => {
          this.registerDepForm.setValue({
            name: data.response.departament.name,
            floor: data.response.departament.floor,
            office: data.response.departament.office,
          });
          this.isEdit = true;
        },
          err => console.log(err)
        );
    } else {
      // this.title = 'Nuevo departament';
      this.isEdit = false;
      
    }
  }
  
 /**
   * Form Validation
   */
  save() {
    console.log(this.registerDepForm);
    this.submitted = true;
    if (this.registerDepForm.invalid) {
      return;
    }

    // Verifico el método a utilizar
    const method = (this.departament.id) ? 'update' : 'create';

    // Cargo los datos del objeto a enviar
    this.departament.name = this.registerDepForm.get('name').value;
    this.departament.floor = this.registerDepForm.get('floor').value;
    this.departament.office = this.registerDepForm.get('office').value;

    this.departamentService[method](this.departament).subscribe(
      (data: IResponse) => {
        if (data.code === 200) {
          this.toastrService.success('Usuario creado con éxito');
          this.activeModal.dismiss();
          // this.updatedepartamentList.emit(true);
        } else {
          this.toastrService.error(data.response.Message);
        }
      },
      err => console.log(err)
    );
  }
}
