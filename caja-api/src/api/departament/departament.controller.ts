import { validate } from 'class-validator';
import { Request, Response } from 'express';


import { DepartamentModel } from './departament.model';

// Entities
import { Departament } from '../../core/database/entities/departament';

class DepartamentController {
  /**
   * Loads all departament from the database
   * @param req Request
   * @param res Response
   */
  public async ctrlList(req: Request, res: Response): Promise<void> {
    try {
      const dep = new DepartamentModel();
      const departament = await dep.list();
      res.status(200).json({ code: 200, response: departament });
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Load departament by id from the database
   * @param req Request
   * @param res Response
   */
  public async ctrlGetOne(req: Request, res: Response): Promise<void> {
    try {
      // Find departament
      const dep = new DepartamentModel();
      const departament: Departament = await dep.getOne(parseInt(req.params.id));

      // Valid the info
      if (departament) {
        res.status(200).json({ code: 200, response: departament });
      } else {
        res.status(404).json({ code: 404, response: 'Departamento no encontrado' });
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Create a departament on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlCreate(req: Request, res: Response): Promise<void> {
    try {
      const dep = new DepartamentModel();

      // Creating a instance of Departament
      const departament = new Departament();
      departament.name = req.body.name;
      departament.floor = req.body.floor;
      departament.office = req.body.office;

      // Validate data departament
      const errorsDepartament = await validate(departament);
      if (errorsDepartament.length > 0) {
        res.status(400).json({ code: 400, response: errorsDepartament });
        return;
      }

      // Return data
      const dataDepartament: Departament = await dep.saveChanges(departament);

      res.status(200).json({ code: 200, response: dataDepartament });
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Update a departament on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlUpdate(req: Request, res: Response): Promise<void> {
    try {
      const dep = new DepartamentModel();

      // Creating a instance of Departament
      const departament = new Departament();
      departament.name = req.body.name;
      departament.floor = req.body.floor;
      departament.office = req.body.office;


      // Validate data Departament
      const errorsDepartament = await validate(departament);
      if (errorsDepartament.length > 0) {
        res.status(400).json({ code: 400, response: errorsDepartament });
        return;
      }

      // Return data
      const dataDepartament: Departament = await dep.saveChanges(departament);

      res.status(200).json({ code: 200, response: dataDepartament });
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Remove a Departament on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlRemove(req: Request, res: Response): Promise<void> {
    try {
      // Search Doctor
      const dep = new DepartamentModel();
      const Departament = await dep.getOne(parseInt(req.params.id));
      if (Departament) {
        // Return data
        const data = await dep.remove(Departament.id);
        res.status(200).json({ code: 200, response: data });
      } else {
        res.status(404).json({ code: 404, response: 'Departament not found' });
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }
}

export const departamentController = new DepartamentController();