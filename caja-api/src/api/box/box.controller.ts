import { validate } from 'class-validator';
import { Request, Response } from 'express';


import { BoxModel } from './box.model';

// Entities
import { Box } from '../../core/database/entities/box';

class BoxController {
  /**
   * Loads all box from the database
   * @param req Request
   * @param res Response
   */
  public async ctrlList(req: Request, res: Response): Promise<void> {
    try {
      const bo = new BoxModel();
      const box = await bo.list();
      res.status(200).json({ code: 200, response: box });
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Load box by id from the database
   * @param req Request
   * @param res Response
   */
  public async ctrlGetOne(req: Request, res: Response): Promise<void> {
    try {
      // Find box
      const bo = new BoxModel();
      const box: Box = await bo.getOne(parseInt(req.params.id));

      // Valid the info
      if (box) {
        res.status(200).json({ code: 200, response: box });
      } else {
        res.status(404).json({ code: 404, response: 'Boxo no encontrado' });
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Create a box on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlCreate(req: Request, res: Response): Promise<void> {
    try {
      const bo = new BoxModel();

      // Creating a instance of Box
      const box = new Box();
      box.money = req.body.money;
      box.observation = req.body.observation;
      box.dayopen = req.body.dayopen;
      box.dayclose = req.body.dayclose;
      box.state = req.body.stateId;

      // Validate data box
      const errorsBox = await validate(box);
      if (errorsBox.length > 0) {
        res.status(400).json({ code: 400, response: errorsBox });
        return;
      }

      // Return data
      const dataBox: Box = await bo.saveChanges(box);

      res.status(200).json({ code: 200, response: dataBox });
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Update a box on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlUpdate(req: Request, res: Response): Promise<void> {
    try {
      const bo = new BoxModel();

      // Creating a instance of Box
      const box = new Box();
      box.money = req.body.money;
      box.observation = req.body.observation;
      box.dayopen = req.body.dayopen;
      box.dayclose = req.body.dayclose;
      box.state = req.body.stateId;


      // Validate data Box
      const errorsBox = await validate(box);
      if (errorsBox.length > 0) {
        res.status(400).json({ code: 400, response: errorsBox });
        return;
      }

      // Return data
      const dataBox: Box = await bo.saveChanges(box);

      res.status(200).json({ code: 200, response: dataBox });
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Remove a Box on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlRemove(req: Request, res: Response): Promise<void> {
    try {
      // Search Doctor
      const bo = new BoxModel();
      const Box = await bo.getOne(parseInt(req.params.id));
      if (Box) {
        // Return data
        const data = await bo.remove(Box.id);
        res.status(200).json({ code: 200, response: data });
      } else {
        res.status(404).json({ code: 404, response: 'Box not found' });
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }
}

export const boxController = new BoxController();
