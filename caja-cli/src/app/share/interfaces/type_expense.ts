export interface TypeExpense {
    id?: number;
    name?: string;
    description?: string;
}