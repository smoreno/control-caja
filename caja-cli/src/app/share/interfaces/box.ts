export interface Box {
    id?: number;
    money?: number;
    observation?: string;
    dayopen?: string;
    dayclose?: string;
    stateId?: string;
}