import { validate } from 'class-validator';
import { Request, Response } from 'express';
import { User } from '../../core/database/entities/user';
import { AdministratorModel } from './administrator.model';

class AdministratorController {
  /**
   * Loads all users from the database
   * @param req Request
   * @param res Response
   */
  public async ctrlList(req: Request, res: Response): Promise<void> {
    try {
      const am = new AdministratorModel();
      const users: User[] = await am.list();
      res.status(200).json({ code: 200, response: users });
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Load user by id from the database
   * @param req Request
   * @param res Response
   */
  public async ctrlGetOne(req: Request, res: Response): Promise<void> {
    try {
      // Find Administrator
      const am = new AdministratorModel();
      const user: User = await am.getOne(parseInt(req.params.id));
      if (user) {
        res.status(200).json({ code: 200, response: user });
      } else {
        res.status(404).json({ code: 404, response: 'Usuario no encontrado' });
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Create a user on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlCreate(req: Request, res: Response): Promise<void> {
    try {
      // Find subcategory
      const am = new AdministratorModel();

      // Creating a instance of User
      const user = new User();
      user.documentNumber = req.body.documentNumber;
      user.firstName = req.body.firstName;
      user.lastName = req.body.lastName;
      user.email = req.body.email;
      user.email = user.email.toLowerCase();
      user.password = req.body.password;
      user.hashPassword();
      user.role = req.body.role;
      user.departament = req.body.departamentId;

      // Validate data
      const errors = await validate(user);
      if (errors.length > 0) {
        res.status(400).json({ code: 400, response: 'Correo electrónico ya registrado en nuestra plataforma' });
        return;
      }

      // Return data
      const data = await am.saveChanges(user);
      res.status(200).json({ code: 200, response: data });
    } catch (error) {
      res.status(500).json({ code: 500, response: 'Correo electrónico ya registrado en nuestra plataforma' });
    }
  }

  /**
   * Update a user on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlUpdate(req: Request, res: Response): Promise<void> {
    try {
      // Find user
      const am = new AdministratorModel();
      const user: User = await am.getOne(parseInt(req.body.id));
      if (!user) {
        res.status(404).json({ code: 404, response: 'Usuario no encontrado' });
        return;
      }

      // Updating a instance of User
      user.firstName = req.body.firstName;
      user.lastName = req.body.lastName;
      user.email = req.body.email;
      user.email = user.email.toLowerCase();
      user.role = req.body.role;
      user.departament = req.body.departamentId;

      // Validate data
      const errors = await validate(user);
      if (errors.length > 0) {
        res.status(400).json({ code: 400, response: errors });
        return;
      }

      // Return data
      const data = await am.saveChanges(user);
      res.status(200).json({ code: 200, response: data });
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Remove a user on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlRemove(req: Request, res: Response): Promise<void> {
    try {
      // Search Administrator
      const am = new AdministratorModel();
      const user = await am.getOne(parseInt(req.params.id));
      if (user) {
        // Return data
        const data = await am.remove(parseInt(req.params.id));
        res.status(200).json({ code: 200, response: data });
      } else {
        res.status(404).json({ code: 404, response: 'Usuario no encontrado' });
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }
}

export const administratorController = new AdministratorController();
