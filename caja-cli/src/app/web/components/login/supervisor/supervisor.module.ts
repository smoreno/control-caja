import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// MODULOS
import { SharedModule } from 'src/app/share/share.module';

// COMPONENTES
import { SupervisorRoutingModule } from './supervisor-routing.module';
import { SupervisorComponent } from './supervisor.component';



@NgModule({
  imports: [
    CommonModule,
    SupervisorRoutingModule,
    SharedModule
    
  ],
  declarations: [
    SupervisorComponent,
  ],
  providers: [],
})
export class SupervisorModule { }
