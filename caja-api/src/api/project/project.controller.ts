import { validate } from 'class-validator';
import { Request, Response } from 'express';


// Entities
import { Project } from '../../core/database/entities/project';
import { ProjectModel } from './project.model';

class ProjectController {
  /**
   * Loads all projects from the database
   * @param req Request
   * @param res Response
   */
  public async ctrlList(req: Request, res: Response): Promise<void> {
    try {
      const proj = new ProjectModel();
      const projects = await proj.list();
      res.status(200).json({ code: 200, response: projects });
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Load project by id from the database
   * @param req Request
   * @param res Response
   */
  public async ctrlGetOne(req: Request, res: Response): Promise<void> {
    try {
      // Find Project
      const proj = new ProjectModel();
      const project: Project = await proj.getOne(parseInt(req.params.id));

      // Valid the info
      if (project) {
        res.status(200).json({ code: 200, response: project });
      } else {
        res.status(404).json({ code: 404, response: 'Factura no encontrada' });
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Create a project on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlCreate(req: Request, res: Response): Promise<void> {
    try {
      const proj = new ProjectModel();

      // Creating a instance of Project
      const project = new Project();
      project.name = req.body.name;
      project.location = req.body.location;
      project.description = req.body.description;

      // Validate data Project
      const errorsProject = await validate(project);
      if (errorsProject.length > 0) {
        res.status(400).json({ code: 400, response: errorsProject });
        return;
      }

      // Return data
      const dataProject: Project = await proj.saveChanges(project);

      res.status(200).json({ code: 200, response: dataProject });
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Update a project on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlUpdate(req: Request, res: Response): Promise<void> {
    try {
      const proj = new ProjectModel();

      // Creating a instance of Project
      const project: Project = await proj.getOne(req.body.Id);

      // Creating a instance of Project
      project.name = req.body.name;
      project.location = req.body.location;
      project.description = req.body.description;
      
      // Validate data Project
      const errorsProject = await validate(project);
      if (errorsProject.length > 0) {
        res.status(400).json({ code: 400, response: errorsProject });
        return;
      }

      // Return data
      const dataProject: Project = await proj.saveChanges(project);

      res.status(200).json({ code: 200, response: dataProject });
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Remove a project on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlRemove(req: Request, res: Response): Promise<void> {
    try {
      // Search Doctor
      const proj = new ProjectModel();
      const project = await proj.getOne(parseInt(req.params.id));
      if (project) {
        // Return data
        const data = await proj.remove(project.id);
        res.status(200).json({ code: 200, response: data });
      } else {
        res.status(404).json({ code: 404, response: 'Project not found' });
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }
}

export const projectController = new ProjectController();