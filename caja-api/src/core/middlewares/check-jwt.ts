import { NextFunction, Request, Response } from 'express';
import { User } from '../database/entities/user';
import { JWTService } from '../services/jwt.service';

export const checkJwt = async (req: Request, res: Response, next: NextFunction) => {
  try {
    // Get the jwt token from the head
    const jwtManage = new JWTService();
    const token = req.headers.authorization;
    let payload: any;
    let currentUser = new User();

    // Decode JWT
    payload = jwtManage.decodeToken(token);

    // If token is not valid, respond with 401 (unauthorized)
    if (payload) {
      // Seach user in the database
      currentUser = await jwtManage.searchUserByIdAndToken(payload.id, token);

      // Valid user exist
      if (currentUser) {
        // Call the next middleware or controller
        next();
      } else {
        res.status(400).json({ code: 400, message: 'Invalid user' });
        return;
      }
    } else {
      res.status(400).json({ code: 400, message: 'Invalid token' });
      return;
    }
  } catch (error) {
    res.status(401).json({ code: 401, message: error });
    return;
  }
};
