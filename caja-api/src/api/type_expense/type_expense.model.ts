import { DeleteResult, getManager } from 'typeorm';
import { TypeExpense } from '../../core/database/entities/type_expense';

export class TypeExpenseModel {
  public async list(): Promise<TypeExpense[]> {
    //  Get repository to operations
    const TypeExpenseRepository = getManager().getRepository(TypeExpense);

    // Return the list of bills
    return await TypeExpenseRepository.find({});
  }

  public async getOne(TypeExpenseId: number): Promise<TypeExpense> {
    //  Get repository to operations
    const TypeExpenseRepository = getManager().getRepository(TypeExpense);

    // Return TypeExpense by id
    return await TypeExpenseRepository.findOne({
      relations: [ 'user'],
      where: { id: TypeExpenseId},
    });
  }

  public async saveChanges(type_expense: TypeExpense): Promise<TypeExpense> {
    return await getManager().getRepository(TypeExpense).save(type_expense);
  }

  public async remove(TypeExpenseId: number): Promise<DeleteResult> {
    return await getManager().getRepository(TypeExpense).delete(TypeExpenseId);
  }
}
