import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

//MODAL
// import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

//Interfaces
import { Box } from 'src/app/share/interfaces/box';
import { IResponse } from 'src/app/share/interfaces/iresponse';

//Services
import { ToastrService } from 'ngx-toastr';
import { BoxService } from 'src/app/core/services/box.service';
import { MovCajaComponent } from '../mov-caja/mov-caja.component';
import { CreateBoxComponent } from '../create-box/create-box.component';

@Component({
  selector: 'app-date-table-detail',
  templateUrl: './date-table-detail.component.html',
  styleUrls: ['./date-table-detail.component.scss']
})
export class DateTableDetailComponent implements OnInit {
  public registerBoxForm: FormGroup;
  public box: Box;
  public isEdit= false;
  public submitted = false;


/**
 * 
 * @param formBuilder 
 * @param toastrService 
 * @param boxService 
 */
  constructor(
    public activeModal: NgbActiveModal,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private toastrService: ToastrService,
    private boxService: BoxService,
  ) { }
  
  close() {
  this.activeModal.dismiss();  
  }

  openCrear(){
    this.modalService.open(MovCajaComponent)   
  }

  ngOnInit() {
    // Build form
    this.registerBoxForm = this.formBuilder.group({
      money: ['', [
        Validators.required,
      ]],
    });    
    this.onSubmit();
  }

  
  get f() { return this.registerBoxForm.controls; }
  
/**
 * Load the data in the user of the update
 * @param id ID of the sent user
   */
  onSubmit(id?: number): void {
                        // console.log(id);
    // I clean the form
    this.registerBoxForm.reset();
    this.submitted = false;
    
    // I create an initial object
    this.box = {} as Box;

    // I verify the property `id`
    if (id) {
      // this.title = 'Editar user';
      // Get of user
      this.boxService.byId(id).subscribe(
        (data: IResponse) => {
          this.registerBoxForm.setValue({
            money: data.response.user.money,
            observation: data.response.user.observation,
            dayopen: data.response.user.dayopen,
            dayclose: data.response.user.dayclose,
            stateId: data.response.user.stateId,        
          });
          
          this.isEdit = true;
        },
        err => console.log(err)
        );
      } else {
        // this.title = 'Nuevo user';
        this.isEdit = false;
    }    
  }

  /**
   * Form Validation
   */
  save() {
    this.submitted = true;
    if (this.registerBoxForm.invalid) {
      return;
    }
    
    // Verifico el método a utilizar
    const method = (this.box.id) ? 'update' : 'create';
    
    // Cargo los datos del objeto a enviar
    this.box.money = this.registerBoxForm.get('money').value;
    // this.box.observation = this.registerBoxForm.get('observation').value;
    // this.box.dayopen = this.registerBoxForm.get('dayopen').value;
    // this.box.dayclose = this.registerBoxForm.get('dayclose').value;
    // this.box.stateId = this.registerBoxForm.get('stateId').value;
    
  this.boxService[method](this.box).subscribe(
      (data: IResponse) => {
        if (data.code === 200) {
        this.toastrService.success('Caja creada con éxito');
        // this.activeModal.dismiss();
      } else {
        this.toastrService.error(data.response.Message);
      }
    },
    err => console.log(err)
    );
      
  }
}
  