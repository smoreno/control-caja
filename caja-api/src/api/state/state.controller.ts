import { validate } from 'class-validator';
import { Request, Response } from 'express';


import { StateModel } from './state.model';

// Entities
import { State } from '../../core/database/entities/state';

class StateController {
  /**
   * Loads all state from the database
   * @param req Request
   * @param res Response
   */
  public async ctrlList(req: Request, res: Response): Promise<void> {
    try {
      const dep = new StateModel();
      const state = await dep.list();
      res.status(200).json({ code: 200, response: state });
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Load state by id from the database
   * @param req Request
   * @param res Response
   */
  public async ctrlGetOne(req: Request, res: Response): Promise<void> {
    try {
      // Find state
      const dep = new StateModel();
      const state: State = await dep.getOne(parseInt(req.params.id));

      // Valid the info
      if (state) {
        res.status(200).json({ code: 200, response: state });
      } else {
        res.status(404).json({ code: 404, response: 'Estado no encontrado' });
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Create a state on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlCreate(req: Request, res: Response): Promise<void> {
    try {
      const dep = new StateModel();

      // Creating a instance of State
      const state = new State();
      state.name = req.body.name;

      // Validate data state
      const errorsState = await validate(state);
      if (errorsState.length > 0) {
        res.status(400).json({ code: 400, response: errorsState });
        return;
      }

      // Return data
      const dataState: State = await dep.saveChanges(state);

      res.status(200).json({ code: 200, response: dataState });
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Update a state on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlUpdate(req: Request, res: Response): Promise<void> {
    try {
      const dep = new StateModel();

      // Creating a instance of State
      const state = new State();
      state.name = req.body.name;

      // Validate data State
      const errorsState = await validate(state);
      if (errorsState.length > 0) {
        res.status(400).json({ code: 400, response: errorsState });
        return;
      }

      // Return data
      const dataState: State = await dep.saveChanges(state);

      res.status(200).json({ code: 200, response: dataState });
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Remove a State on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlRemove(req: Request, res: Response): Promise<void> {
    try {
      // Search Doctor
      const dep = new StateModel();
      const State = await dep.getOne(parseInt(req.params.id));
      if (State) {
        // Return data
        const data = await dep.remove(State.id);
        res.status(200).json({ code: 200, response: data });
      } else {
        res.status(404).json({ code: 404, response: 'State not found' });
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }
}

export const stateController = new StateController();