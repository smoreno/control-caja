import { Router } from 'express';
import { checkJwt } from '../../core/middlewares/check-jwt';
import { administratorController } from './administrator.controller';

class AdministratorRoutes {
  public router: Router = Router();

  constructor() {
    this.config();
  }

  public config(): void {
    this.router.get('/', checkJwt, administratorController.ctrlList);
    this.router.get('/:id([0-9]+)', checkJwt, administratorController.ctrlGetOne);
    this.router.post('/', checkJwt, administratorController.ctrlCreate);
    this.router.put('/', checkJwt, administratorController.ctrlUpdate);
    this.router.delete('/:id([0-9]+)', checkJwt, administratorController.ctrlRemove);
  }
}

export default new AdministratorRoutes().router;
