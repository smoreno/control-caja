export interface IMail {
  from: string;
  priority: string;
  encoding: string;
  to: string;
  subject: string;
  html: string;
}
