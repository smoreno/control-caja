export interface Movement {
    id?: number;
    numfact?: number;
    description?: string;
    dayfact?: number;
    amountfact?: number;
    attached?: string;
    residue?: number;
    boxId?: number;
    responsableId?: number;
    projectId?: number;
    expenseId?: number;
}