# Project
API of Dr. Froilan Paez project

## Commands
* npm run watch --> Mode on development
* npm run tests --> Run all tests
* npm run docs --> Generate the code documentation

## Libraries
* Node --> https://nodejs.org
* Express --> http://expressjs.com
* TypeORM --> https://typeorm.io
  * class-validator --> https://github.com/typestack/class-validator
  * MySQL --> https://dev.mysql.com/doc/refman/8.0/en/
* TypeDoc --> https://typedoc.org/guides/doccomments/
* ChaiHttp --> https://www.chaijs.com/plugins/chai-http/

## Process
* Entity
* Route -> Swagger Docs
* Controller -> Docs
* Model -> Docs
* Test
