import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './web/components/login/login.component';
import { NotFoundComponent } from './web/components/not-found/not-found.component';

const routes: Routes = [
  { path: '', redirectTo: 'auth', pathMatch: 'full' },
  { path: 'auth', component: LoginComponent },
  {
    path: 'administrador', loadChildren: () => import('./web/components/login/administrador/home/administrador.module').then
    (m => m.AdministradorModule)
  },
  {
    path: 'supervisor', loadChildren: () => import('./web/components/login/supervisor/supervisor.module').then
    (m => m.SupervisorModule)
  },
  {
    path: 'operador', loadChildren: () => import('./web/components/login/operador/operador.module').then
    (m => m.OperadorModule)
  },
  { path: '**', component: NotFoundComponent},
  ];

@NgModule({
  imports: [ RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
