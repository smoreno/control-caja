import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';
import { ToastrService } from 'ngx-toastr';

//MODAL
import { NgbActiveModal, NgbModalRef, NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';


// Interfaces
import { Project } from 'src/app/share/interfaces/project';
import { IResponse } from 'src/app/share/interfaces/iresponse';

// Services
import { ProjectService } from 'src/app/core/services/project.service';

@Component({
  selector: 'app-business-detail',
  templateUrl: './business-detail.component.html',
  styleUrls: ['./business-detail.component.scss']
})
export class BusinessDetailComponent implements OnInit {
  public registerEmpForm: FormGroup;
  public project: Project;
  public isEdit= false;
  public submitted = false;
  
  /**
   * Class constructor
   * @param activeModal 
   * @param formBuilder 
   * @param lsService 
   * @param toastrService 
   * @param projectService 
   * @param router 
   */
  constructor(
    public activeModal: NgbActiveModal, // MODAL    
    private formBuilder: FormBuilder,
    // private lsService: LocalStorageService,
    private toastrService: ToastrService,
    private projectService: ProjectService,
    private router: Router) {}

    close() {
      this.activeModal.dismiss();
    }

  ngOnInit() {

    // Build form
    this.registerEmpForm = this.formBuilder.group({
      name: ['', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(30),
      ]],
      description: ['', [
        Validators.minLength(20),
      ]],
      location: ['', [
        Validators.required,
        Validators.minLength(10),
      ]],
    });
    this.onSubmit();
  }

  get f() { return this.registerEmpForm.controls; }
 
/**
   * Load the data in the project of the update
   * @param id ID of the sent project
   */
  onSubmit(id?: number): void {
    console.log(this.registerEmpForm);
    // I clean the form
    this.registerEmpForm.reset();
    this.submitted = false;

    // I create an initial object
    this.project = {} as Project;

    // I verify the property `id`
    if (id) {
      // this.title = 'Editar project';
      // Get of project
      this.projectService.byId(id).subscribe(
        (data: IResponse) => {
          this.registerEmpForm.setValue({
            name: data.response.project.name,
            location: data.response.project.location,
            description: data.response.project.description,
          });
          this.isEdit = true;
        },
          err => console.log(err)
        );
    } else {
      // this.title = 'Nuevo project';
      this.isEdit = false;
      
    }
  }

 /**
   * Form Validation
   */
  save() {
    console.log(this.registerEmpForm);
    this.submitted = true;
    if (this.registerEmpForm.invalid) {
      return;
    }

    // Verifico el método a utilizar
    const method = (this.project.id) ? 'update' : 'create';

    // Cargo los datos del objeto a enviar
    this.project.name = this.registerEmpForm.get('name').value;
    this.project.location = this.registerEmpForm.get('location').value;
    this.project.description = this.registerEmpForm.get('description').value;

    this.projectService[method](this.project).subscribe(
      (data: IResponse) => {
        if (data.code === 200) {
          this.toastrService.success('Usuario creado con éxito');
          this.activeModal.dismiss();
          // this.updateprojectList.emit(true);
        } else {
          this.toastrService.error(data.response.Message);
        }
      },
      err => console.log(err)
    );
  }

}