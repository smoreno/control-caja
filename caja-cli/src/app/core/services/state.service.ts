import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { State } from 'src/app/share/interfaces/state';

@Injectable()
export class StateService {
  /**
   * Service builder stateService
   * @param http HTTP request service
   */
  constructor(private http: HttpClient) {}

  /**
   * Get all doctors
   */
  liststates() {
    return this.http.get(`${ environment.apiRoot }/state`);
  }
  
  /**
   * Get one state for detail
   * @param id state to find
   */
  detail(id: number) {
    return this.http.get(`${ environment.apiRoot }/state/${ id }/detail`);
  }

  /**
   * Get one state
   * @param id state for consult
   */
  byId(id: number) {
    return this.http.get(`${ environment.apiRoot }/state/${ id }`);
  }

  /**
   * Create a new state
   * @param state Object of the new state
   */
  create(state: State) {
    return this.http.post(`${ environment.apiRoot }/state`, state);
  }

  /**
   * Update a state
   * @param state Object of the state to update
   */
  update(state: State) {
    return this.http.put(`${ environment.apiRoot }/state`, state);
  }

  /**
   * Delete a state
   * @param id state for delete
   */
  removestate(id: number) {
    return this.http.delete(`${ environment.apiRoot }/state/${ id }`);
  }
}