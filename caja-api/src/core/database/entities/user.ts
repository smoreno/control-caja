import * as bcrypt from 'bcrypt';
import { IsEmail, MaxLength } from 'class-validator';
import { Column, Entity, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import Role from '../helpers/role';
import { Departament } from './departament';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ type: 'bigint', unique: true })
  public documentNumber: number;

 // Claves Foraneas

 @ManyToOne((type: any) => Departament, { onDelete: 'CASCADE' })
 public departament: Departament;

 // Fin de la clave Forenea
 
  @Column({ type: 'enum', enum: Role })
  public role: string;

  @Column()
  @MaxLength(20, { message: 'First Name is too long' })
  public firstName: string;

  @Column()
  @MaxLength(20, { message: 'Last Name is too long' })
  public lastName: string;

  // @Column({ nullable: true })
  // public photo: string;

  @Column({unique: true})
  @IsEmail()
  public email: string;

  @Column()
  public password: string;

  @Column({ nullable: true })
  public token: string;

  public hashPassword() {
    this.password = bcrypt.hashSync(this.password, 8);
  }

  public checkIfUnencryptedPasswordIsValid(unencryptedPassword: string) {
    return bcrypt.compareSync(unencryptedPassword, this.password);
  }
}
