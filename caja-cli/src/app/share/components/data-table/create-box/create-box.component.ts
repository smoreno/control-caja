import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

//MODAL
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';


//INTERFACES
import { Box } from 'src/app/share/interfaces/box';
import { State } from 'src/app/share/interfaces/state';

//SERVICES
import { ToastrService } from 'ngx-toastr';
import { BoxService } from 'src/app/core/services/box.service';
import { StateService } from 'src/app/core/services/state.service';
import { IResponse } from 'src/app/share/interfaces/iresponse';

@Component({
  selector: 'app-create-box',
  templateUrl: './create-box.component.html',
  styleUrls: ['./create-box.component.scss']
})
export class CreateBoxComponent implements OnInit {
  public crearBoxForm: FormGroup;
  public box: Box;
  public state: State;
  public isEdit= false;
  public submitted = false;


  constructor(
    public activeModal: NgbActiveModal, // MODAL
    private formBuilder: FormBuilder,
    private toastrService: ToastrService,
    private boxService: BoxService,
    private stateService: StateService,
  ) { }

  close() {
    this.activeModal.dismiss();
  }

  ngOnInit() {
    // Build form
    this.crearBoxForm = this.formBuilder.group({
      money: ['', [
        Validators.required,
      ]],
    });
    this.onSubmit();
  }

  get f() { return this.crearBoxForm.controls; }

  onSubmit(id?: number): void {
    console.log(id);
    // I clean the form
    this.crearBoxForm.reset();
    this.submitted = false;

    // I create an initial object
    this.box = {} as Box;

    // I verify the property `id`
    if (id) {
      // this.title = 'Editar user';
      // Get of user
      this.boxService.byId(id).subscribe(
        (data: IResponse) => {
          this.crearBoxForm.setValue({
            money: data.response.user.money,
            // observation: data.response.user.observation,
            // dayopen: data.response.user.dayopen,
            // dayclose: data.response.user.dayclose,
            // stateId: data.response.user.stateId,         
          });

          this.isEdit = true;
        },
          err => console.log(err)
        );
    } else {
      this.isEdit = false;
    }    
  }

  /**
   * Form Validation
   */
  save() {
    this.submitted = true;
    if (this.crearBoxForm.invalid) {
      return;
    }
      console.log(this.crearBoxForm);

    // Verifico el método a utilizar
    const method = (this.box.id) ? 'update' : 'create';

    // Cargo los datos del objeto a enviar
    this.box.money = this.crearBoxForm.get('money').value;
    // this.box.observation = this.crearBoxForm.get('observation').value;
    // this.box.dayopen = this.crearBoxForm.get('dayopen').value;
    // this.box.dayclose = this.crearBoxForm.get('dayclose').value;
    // this.box.stateId = this.crearBoxForm.get('stateId').value;

    this.boxService[method](this.box).subscribe(
        (data: IResponse) => {
        if (data.code === 200) {
          this.toastrService.success('Box creado con éxito');
          this.activeModal.dismiss();
        } else {
          this.toastrService.error(data.response.Message);
        }
      },
      err => console.log(err)
    );    
  }

}
