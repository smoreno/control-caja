import { DeleteResult, getManager } from 'typeorm';
import { Movement } from '../../core/database/entities/movement';

export class MovementModel {
  public async list(): Promise<Movement[]> {
    //  Get repository to operations
    const MovementRepository = getManager().getRepository(Movement);

    // Return the list of bills
    return await MovementRepository.find({});
  }

  public async getOne(MovementId: number): Promise<Movement> {
    //  Get repository to operations
    const MovementRepository = getManager().getRepository(Movement);

    // Return Movement by id
    return await MovementRepository.findOne({
      relations: [ 'user'],
      where: { id: MovementId},
    });
  }

  public async saveChanges(movement: Movement): Promise<Movement> {
    return await getManager().getRepository(Movement).save(movement);
  }

  public async remove(MovementId: number): Promise<DeleteResult> {
    return await getManager().getRepository(Movement).delete(MovementId);
  }
}
