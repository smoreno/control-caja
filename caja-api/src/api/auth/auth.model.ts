import { getManager } from 'typeorm';
import { User } from '../../core/database/entities/user';

export class AuthModel {
  public async logout(userToken: string): Promise<boolean> {
    //  Get repository to operations
    const userRepository = getManager().getRepository(User);

    // Return user by id and token
    const userFound = await userRepository.findOne({ where: { token: userToken } });

    // Delete token of the database
    let deleteToken;
    if (userFound) {
      userFound.token = null;
      deleteToken = await userRepository.save(userFound);
    } else {
      deleteToken = false;
    }

    // Return result
    return (userFound && deleteToken) ? true : false;
  }

  public async changePassword(userToken: string, newPassword: string): Promise<boolean> {
    //  Get repository to operations
    const userRepository = getManager().getRepository(User);

    // Return user by id and token
    const userFound = await userRepository.findOne({ where: { token: userToken } });

    // Delete token of the database
    let userChangePass;
    if (userFound) {
      userFound.password = newPassword;
      userFound.hashPassword();
      userChangePass = await userRepository.save(userFound);
    } else {
      userChangePass = false;
    }

    // Return result
    return (userFound && userChangePass) ? true : false;
  }

  public async verifyEmail(userEmail: string): Promise<User> {
    //  Get repository to operations
    const userRepository = getManager().getRepository(User);

    // Return user by email and password
    return await userRepository.findOne({ where: { email: userEmail } });
  }
}
