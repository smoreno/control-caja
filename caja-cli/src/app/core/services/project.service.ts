import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Project } from 'src/app/share/interfaces/project';

@Injectable()
export class ProjectService {
  /**
   * Service builder projectService
   * @param http HTTP request service
   */
  constructor(private http: HttpClient) {}

  /**
   * Get all doctors
   */
  listprojects() {
    return this.http.get(`${ environment.apiRoot }/project`);
  }
  
  /**
   * Get one project for detail
   * @param id project to find
   */
  detail(id: number) {
    return this.http.get(`${ environment.apiRoot }/project/${ id }/detail`);
  }

  /**
   * Get one project
   * @param id project for consult
   */
  byId(id: number) {
    return this.http.get(`${ environment.apiRoot }/project/${ id }`);
  }

  /**
   * Create a new project
   * @param project Object of the new project
   */
  create(project: Project) {
    return this.http.post(`${ environment.apiRoot }/project`, project);
  }

  /**
   * Update a project
   * @param project Object of the project to update
   */
  update(project: Project) {
    return this.http.put(`${ environment.apiRoot }/project`, project);
  }

  /**
   * Delete a project
   * @param id project for delete
   */
  removeproject(id: number) {
    return this.http.delete(`${ environment.apiRoot }/project/${ id }`);
  }
}
