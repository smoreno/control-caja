import { IEnvironment } from '../../interfaces/i-environment';

export const development: IEnvironment = {
  ENVIRONMENT: 'development',
  NODE_PORT: '3000',
  NODE_SERVER: 'https://',
  CLIENT_SERVER: 'https://',
  DB_PORT: '3306',
  DB_HOST: 'us-iron-auto-dca-02-b.cleardb.net',
  DB_NAME: 'system_box',
  DB_USER: 'root',
  DB_PASSWORD: 'root',
  DB_CHARSET: 'utf8_general_ci',
  DB_SYNCHRONIZE: true,
  DB_LOGGING: false,
};
