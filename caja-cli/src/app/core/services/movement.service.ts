import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Movement } from 'src/app/share/interfaces/movement';

@Injectable()
export class MovementService {
  /**
   * Service builder movementService
   * @param http HTTP request service
   */
  constructor(private http: HttpClient) {}

  /**
   * Get all doctors
   */
  listmovements() {
    return this.http.get(`${ environment.apiRoot }/movement`);
  }
  
  /**
   * Get one movement for detail
   * @param id movement to find
   */
  detail(id: number) {
    return this.http.get(`${ environment.apiRoot }/movement/${ id }/detail`);
  }

  /**
   * Get one movement
   * @param id movement for consult
   */
  byId(id: number) {
    return this.http.get(`${ environment.apiRoot }/movement/${ id }`);
  }

  /**
   * Create a new movement
   * @param movement Object of the new movement
   */
  create(movement: Movement) {
    return this.http.post(`${ environment.apiRoot }/movement`, movement);
  }

  /**
   * Update a movement
   * @param movement Object of the movement to update
   */
  update(movement: Movement) {
    return this.http.put(`${ environment.apiRoot }/movement`, movement);
  }

  /**
   * Delete a movement
   * @param id movement for delete
   */
  removemovement(id: number) {
    return this.http.delete(`${ environment.apiRoot }/movement/${ id }`);
  }
}
