import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Box } from 'src/app/share/interfaces/box';

@Injectable()
export class BoxService {
  /**
   * Service builder BoxService
   * @param http HTTP request service
   */
  constructor(private http: HttpClient) {}

  /**
   * Get all doctors
   */
  listBoxs() {
    return this.http.get(`${ environment.apiRoot }/box`);
  }

  /**
   * Get one user for setting
   * @param id user Setting to find
   */
  allSettingByuserId(id: number) {
    return this.http.get(`${ environment.apiRoot }/box/${ id }`);
  }

  /**
   * Update a profile
   * @param box Object of the user to update
   */
  updateProfile(box: Box) {
    return this.http.patch(`${ environment.apiRoot }/box/update-profile`, box);
  }

  /**
   * Update a notifications
   * @param box Object of the user to update
   */
  updateNotifications(box: Box) {
    return this.http.patch(`${ environment.apiRoot }/box/update-notifications`, box);
  }

  /**
   * Get one user for detail
   * @param id user to find
   */
  detail(id: number) {
    return this.http.get(`${ environment.apiRoot }/box/${ id }/detail`);
  }

  /**
   * Get one user
   * @param id user for consult
   */
  byId(id: number) {
    return this.http.get(`${ environment.apiRoot }/box/${ id }`);
  }

  /**
   * Create a new user
   * @param box Object of the new user
   */
  create(box: Box) {
    return this.http.post(`${ environment.apiRoot }/box`, box);
  }

  /**
   * Update a user
   * @param box Object of the user to update
   */
  update(box: Box) {
    return this.http.put(`${ environment.apiRoot }/box`, box);
  }

  /**
   * Delete a user
   * @param id user for delete
   */
  removeuser(id: number) {
    return this.http.delete(`${ environment.apiRoot }/box/${ id }`);
  }
}
