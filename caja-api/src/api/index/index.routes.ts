import { Router } from 'express';
import { indexController } from './index.controller';

class IndexRoutes {
  public router: Router = Router();

  constructor() {
    this.config();
  }

  public config(): void {
    this.router.get('/', indexController.list);
  }
}

export default new IndexRoutes().router;
