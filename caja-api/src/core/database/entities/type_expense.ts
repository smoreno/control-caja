// import * as bcrypt from 'bcrypt';
// import { IsEmail, MaxLength } from 'class-validator';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
// import Role from '../helpers/role';

@Entity()

export class TypeExpense {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public name: string;

  @Column()
  public description: string;
}
