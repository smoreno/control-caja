export interface IEnvironment {
  ENVIRONMENT: string;
  NODE_PORT: string;
  NODE_SERVER: string;
  CLIENT_SERVER: string;
  DB_PORT: string;
  DB_HOST: string;
  DB_NAME: string;
  DB_USER: string;
  DB_PASSWORD: string;
  DB_CHARSET: string;
  DB_SYNCHRONIZE: boolean;
  DB_LOGGING: boolean;
}
