import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Administrator } from 'src/app/share/interfaces/administrator';

@Injectable()
export class AdministratorService {
  /**
   * Service builder AdministratorService
   * @param http HTTP request service
   */
  constructor(private http: HttpClient) {}

  /**
   * Get all doctors
   */
  listAdministrators() {
    return this.http.get(`${ environment.apiRoot }/administrator`);
  }

  /**
   * Get one user for setting
   * @param id user Setting to find
   */
  allSettingByuserId(id: number) {
    return this.http.get(`${ environment.apiRoot }/administrator/${ id }`);
  }

  /**
   * Update a profile
   * @param administrator Object of the user to update
   */
  updateProfile(administrator: Administrator) {
    return this.http.patch(`${ environment.apiRoot }/administrator/update-profile`, administrator);
  }

  /**
   * Update a notifications
   * @param administrator Object of the user to update
   */
  updateNotifications(administrator: Administrator) {
    return this.http.patch(`${ environment.apiRoot }/administrator/update-notifications`, administrator);
  }

  /**
   * Get one user for detail
   * @param id user to find
   */
  detail(id: number) {
    return this.http.get(`${ environment.apiRoot }/administrator/${ id }/detail`);
  }

  /**
   * Get one user
   * @param id user for consult
   */
  byId(id: number) {
    return this.http.get(`${ environment.apiRoot }/administrator/${ id }`);
  }

  /**
   * Create a new user
   * @param administrator Object of the new user
   */
  create(administrator: Administrator) {
    return this.http.post(`${ environment.apiRoot }/administrator`, administrator);
  }

  /**
   * Update a user
   * @param administrator Object of the user to update
   */
  update(administrator: Administrator) {
    return this.http.put(`${ environment.apiRoot }/administrator`, administrator);
  }

  /**
   * Delete a user
   * @param id user for delete
   */
  removeuser(id: number) {
    return this.http.delete(`${ environment.apiRoot }/administrator/${ id }`);
  }
}
