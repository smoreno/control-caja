import { Router } from 'express';
import { checkJwt } from '../../core/middlewares/check-jwt';
import { type_expenseController } from './type_expense.controller';

class TypeExpenseRoutes {
  public router: Router = Router();

  constructor() {
    this.config();
  }

  public config(): void {
    this.router.get('/', checkJwt, type_expenseController.ctrlList);
    this.router.get('/:id([0-9]+)', checkJwt, type_expenseController.ctrlGetOne);
    this.router.post('/', checkJwt, type_expenseController.ctrlCreate);
    this.router.patch('/', checkJwt, type_expenseController.ctrlUpdate);
    this.router.delete('/:id([0-9]+)', checkJwt, type_expenseController.ctrlRemove);
  }
}

export default new TypeExpenseRoutes().router;