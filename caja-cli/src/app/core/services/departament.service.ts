import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Departament } from 'src/app/share/interfaces/departament';

@Injectable()
export class DepartamentService {
  /**
   * Service builder departamentService
   * @param http HTTP request service
   */
  constructor(private http: HttpClient) {}

  /**
   * Get all doctors
   */
  listdepartaments() {
    return this.http.get(`${ environment.apiRoot }/departament`);
  }

  /**
   * Get one departament for detail
   * @param id departament to find
   */
  detail(id: number) {
    return this.http.get(`${ environment.apiRoot }/departament/${ id }/detail`);
  }

  /**
   * Get one departament
   * @param id departament for consult
   */
  byId(id: number) {
    return this.http.get(`${ environment.apiRoot }/departament/${ id }`);
  }

  /**
   * Create a new departament
   * @param departament Object of the new departament
   */
  create(departament: Departament) {
    return this.http.post(`${ environment.apiRoot }/departament`, departament);
  }

  /**
   * Update a departament
   * @param departament Object of the departament to update
   */
  update(departament: Departament) {
    return this.http.put(`${ environment.apiRoot }/departament`, departament);
  }

  /**
   * Delete a departament
   * @param id departament for delete
   */
  removedepartament(id: number) {
    return this.http.delete(`${ environment.apiRoot }/departament/${ id }`);
  }
}
