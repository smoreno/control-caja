import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

//Services
import { AuthService } from '../services/auth.service';
import { LocalStorageService } from '../services/local-storage.service';
import { ToastrService } from 'ngx-toastr';


import { catchError } from 'rxjs/operators';

//Routes
import { Router } from '@angular/router';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(
    private authService: AuthService,
    private lsService: LocalStorageService,
    private toastrService: ToastrService,
    private router: Router) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(err => {
      if (err.status === 401) {
        this.authService.logout();
        this.lsService.clearStorage();
        this.toastrService.error('Ocurrio un error inesperado');
        this.router.navigate(['/auth']);
      } else {
        this.toastrService.error(err.error.response);
      }

      const error = err.error.message || err.statusText;
      return throwError(error);
    }));
  }
}
