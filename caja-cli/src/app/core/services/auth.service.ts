import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Authentication } from 'src/app/share/interfaces/authentication';
import { environment } from 'src/environments/environment';

@Injectable()
export class AuthService {
  constructor(private http: HttpClient) {}

  login(auth: Authentication){
    return this.http.post(`${ environment.apiRoot}/auth/login`, auth);
  }

  logout(){
    return this.http.delete(`${ environment.apiRoot}/auth/logout`);
  }
}
