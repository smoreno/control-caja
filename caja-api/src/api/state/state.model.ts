import { DeleteResult, getManager } from 'typeorm';
import { State } from '../../core/database/entities/state';

export class StateModel {
  public async list(): Promise<State[]> {
    //  Get repository to operations
    const StateRepository = getManager().getRepository(State);

    // Return the list of bills
    return await StateRepository.find({});
  }

  public async getOne(StateId: number): Promise<State> {
    //  Get repository to operations
    const StateRepository = getManager().getRepository(State);

    // Return State by id
    return await StateRepository.findOne({
      relations: [ 'user'],
      where: { id: StateId},
    });
  }

  public async saveChanges(state: State): Promise<State> {
    return await getManager().getRepository(State).save(state);
  }

  public async remove(StateId: number): Promise<DeleteResult> {
    return await getManager().getRepository(State).delete(StateId);
  }
}
