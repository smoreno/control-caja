import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ToastrModule } from 'ngx-toastr';

// Modales
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import {NgbModal, ModalDismissReasons, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';


// Directives

// Components
import { DataTableComponent } from './components/data-table/data-table-list/data-table.component';
import { DateTableDetailComponent } from './components/data-table/date-table-detail/date-table-detail.component';
import { MovCajaComponent } from './components/data-table/mov-caja/mov-caja.component';
import { CreateBoxComponent } from './components/data-table/create-box/create-box.component';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    BsDropdownModule.forRoot(),
    ToastrModule.forRoot({
      timeOut: 2000,
      positionClass: 'toast-top-right',
      progressBar: true,
      preventDuplicates: true,
      resetTimeoutOnDuplicate: true,
      closeButton: true,
    }),
  ],
  exports: [
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    DataTableComponent,
    DateTableDetailComponent,
    MovCajaComponent,
    CreateBoxComponent,
  ],
  declarations: [
    DataTableComponent,
    DateTableDetailComponent,
    MovCajaComponent,
    CreateBoxComponent,
  ],
  entryComponents: [
    CreateBoxComponent,
    MovCajaComponent,
    DateTableDetailComponent,
  ]
})
export class SharedModule {}