import { Component, OnInit } from '@angular/core';

// MODAL
import {NgbModal } from '@ng-bootstrap/ng-bootstrap';

//Component
import { BusinessDetailComponent } from '../business-detail/business-detail.component';

// Interfaces
import { IResponse } from 'src/app/share/interfaces/iresponse';

//SERVICES
import { ToastrService } from 'ngx-toastr';
import { ProjectService } from 'src/app/core/services/project.service';

@Component({
  selector: 'app-business-list',
  templateUrl: './business-list.component.html',
  styleUrls: ['./business-list.component.scss']
})
export class BusinessListComponent implements OnInit {
    closeResult: string;
    public administrator: any;
    public data: any;
    public usuarios: any;

  constructor(
    private modalService: NgbModal,
    private projectService: ProjectService,
    private toastrService: ToastrService,
    ) { }

  ngOnInit() {
    this.loadTabla();
  }

  open() {
    this.modalService.open(BusinessDetailComponent);
  }

  loadTabla() {
    // Get all countries
    this.projectService.listprojects().subscribe(
      (data: IResponse) => {
        this.toastrService.success('Datos cargados con éxito');
        this.data = data.response;
        console.log(this.data);
      },
      err => console.log(err)

    );
  }

}
