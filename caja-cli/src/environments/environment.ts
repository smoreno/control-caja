export const environment = {
  production: false,
  apiRoot: 'http://localhost:3000',
  messages: {
    success: 'Operación realizada con éxito',
    error: 'Ocurrió un error inesperado, por favor intente nuevamente',
    deleteSucces: 'Se ha eliminado exitosamente este registro',
    withoutData: 'Sin registros para mostrar',
    logout: 'Cierre de sesión exitoso',
    login: 'Inicio de sesión exitoso',
    successData: 'Data cargados exitosamente',
  }
};
