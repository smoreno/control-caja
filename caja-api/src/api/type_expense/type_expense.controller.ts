import { validate } from 'class-validator';
import { Request, Response } from 'express';


import { TypeExpenseModel } from './type_expense.model';

// Entities
import { TypeExpense } from '../../core/database/entities/type_expense';

class TypeExpenseController {
  /**
   * Loads all type_expense from the database
   * @param req Request
   * @param res Response
   */
  public async ctrlList(req: Request, res: Response): Promise<void> {
    try {
      const dep = new TypeExpenseModel();
      const type_expense = await dep.list();
      res.status(200).json({ code: 200, response: type_expense });
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Load type_expense by id from the database
   * @param req Request
   * @param res Response
   */
  public async ctrlGetOne(req: Request, res: Response): Promise<void> {
    try {
      // Find type_expense
      const dep = new TypeExpenseModel();
      const type_expense: TypeExpense = await dep.getOne(parseInt(req.params.id));

      // Valid the info
      if (type_expense) {
        res.status(200).json({ code: 200, response: type_expense });
      } else {
        res.status(404).json({ code: 404, response: 'TypeExpenseo no encontrado' });
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Create a type_expense on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlCreate(req: Request, res: Response): Promise<void> {
    try {
      const dep = new TypeExpenseModel();

      // Creating a instance of TypeExpense
      const type_expense = new TypeExpense();
      type_expense.name = req.body.name;
      type_expense.description = req.body.descriptiion;

      // Validate data type_expense
      const errorsTypeExpense = await validate(type_expense);
      if (errorsTypeExpense.length > 0) {
        res.status(400).json({ code: 400, response: errorsTypeExpense });
        return;
      }

      // Return data
      const dataTypeExpense: TypeExpense = await dep.saveChanges(type_expense);

      res.status(200).json({ code: 200, response: dataTypeExpense });
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Update a type_expense on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlUpdate(req: Request, res: Response): Promise<void> {
    try {
      const dep = new TypeExpenseModel();

      // Creating a instance of TypeExpense
      const type_expense = new TypeExpense();
      type_expense.name = req.body.name;
      type_expense.description = req.body.descriptiion;


      // Validate data TypeExpense
      const errorsTypeExpense = await validate(type_expense);
      if (errorsTypeExpense.length > 0) {
        res.status(400).json({ code: 400, response: errorsTypeExpense });
        return;
      }

      // Return data
      const dataTypeExpense: TypeExpense = await dep.saveChanges(type_expense);

      res.status(200).json({ code: 200, response: dataTypeExpense });
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Remove a TypeExpense on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlRemove(req: Request, res: Response): Promise<void> {
    try {
      // Search Doctor
      const dep = new TypeExpenseModel();
      const TypeExpense = await dep.getOne(parseInt(req.params.id));
      if (TypeExpense) {
        // Return data
        const data = await dep.remove(TypeExpense.id);
        res.status(200).json({ code: 200, response: data });
      } else {
        res.status(404).json({ code: 404, response: 'TypeExpense not found' });
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }
}

export const type_expenseController = new TypeExpenseController();