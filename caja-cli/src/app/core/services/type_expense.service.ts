import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { TypeExpense } from 'src/app/share/interfaces/type_expense';

@Injectable()
export class TypeExpenseService {
  /**
   * Service builder type_expenseService
   * @param http HTTP request service
   */
  constructor(private http: HttpClient) {}

  /**
   * Get all doctors
   */
  listtype_expenses() {
    return this.http.get(`${ environment.apiRoot }/type_expense`);
  }
  
  /**
   * Get one type_expense for detail
   * @param id type_expense to find
   */
  detail(id: number) {
    return this.http.get(`${ environment.apiRoot }/type_expense/${ id }/detail`);
  }

  /**
   * Get one type_expense
   * @param id type_expense for consult
   */
  byId(id: number) {
    return this.http.get(`${ environment.apiRoot }/type_expense/${ id }`);
  }

  /**
   * Create a new type_expense
   * @param type_expense Object of the new type_expense
   */
  create(type_expense: TypeExpense) {
    return this.http.post(`${ environment.apiRoot }/type_expense`, type_expense);
  }

  /**
   * Update a type_expense
   * @param type_expense Object of the type_expense to update
   */
  update(type_expense: TypeExpense) {
    return this.http.put(`${ environment.apiRoot }/type_expense`, type_expense);
  }

  /**
   * Delete a type_expense
   * @param id type_expense for delete
   */
  removetype_expense(id: number) {
    return this.http.delete(`${ environment.apiRoot }/type_expense/${ id }`);
  }
}