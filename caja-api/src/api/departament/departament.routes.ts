import { Router } from 'express';
import { checkJwt } from '../../core/middlewares/check-jwt';
import { departamentController } from './departament.controller';

class DepartamentRoutes {
  public router: Router = Router();

  constructor() {
    this.config();
  }

  public config(): void {
    this.router.get('/', checkJwt, departamentController.ctrlList);
    this.router.get('/:id([0-9]+)', checkJwt, departamentController.ctrlGetOne);
    this.router.post('/', checkJwt, departamentController.ctrlCreate);
    this.router.patch('/', checkJwt, departamentController.ctrlUpdate);
    this.router.delete('/:id([0-9]+)', checkJwt, departamentController.ctrlRemove);
  }
}

export default new DepartamentRoutes().router;