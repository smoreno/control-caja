// import * as bcrypt from 'bcrypt';
// import { IsEmail, MaxLength } from 'class-validator';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Box } from './box';
import { Project } from './project';
import { TypeExpense } from './type_expense';
import { User } from './user';

@Entity()
export class Movement {
  @PrimaryGeneratedColumn()
  public id: number;
    // Claves foraneas
  @ManyToOne((type: any) => Box, { onDelete: 'CASCADE' })
  public box: Box;

  @ManyToOne((type: any) => User, { onDelete: 'CASCADE' })
  public responsable: User;

  @ManyToOne((type: any) => Project, { onDelete: 'CASCADE' })
  public project: Project;

  @ManyToOne((type: any) => TypeExpense, { onDelete: 'CASCADE' })
  public expense: TypeExpense;
    // Fin de la claves foraneas
  @Column()
  public numfact: number;

  @Column()
  public description: string;

  @Column()
  public dayfact: string;

  @Column()
  public amountfact: string;

  @Column()
  public attached: string;

  @Column()
  public residue: string;
}
