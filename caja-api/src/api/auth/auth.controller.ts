import { Request, Response } from 'express';
import { checkEnvironment } from '../../core/middlewares/check-environment';
import { JWTService } from '../../core/services/jwt.service';
import { EmailService } from '../../core/services/mail.service';
import { AuthModel } from './auth.model';

class AuthController {
  /**
   * Validate user an password on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlLogin(req: Request, res: Response): Promise<void> {
    try {
      const authModel = new AuthModel();
     //const doctorModel = new DoctorModel();

      // Check if username and password are set
      if (!req.body.email && !req.body.password) {
        res.status(400).json({ code: 400, response: 'Datos inválidos, es necesario los datos de correo electrónico y contraseña' });
        return;
      }

      // Get user from database
      const user = await authModel.verifyEmail(req.body.email);
      if (!user) {
        res.status(400).json({ code: 400, response: 'Correo electrónico o contraseña inválidos' });
        return;
      }

      // Check if encrypted password match
      if (!user.checkIfUnencryptedPasswordIsValid(req.body.password)) {
        res.status(400).json({ code: 400, response: 'Correo electrónico o contraseña inválidos' });
        return;
      }

      // Generate and save token on database
      const jwtManage = new JWTService();
      user.token = jwtManage.generateToken(user);
      const userWithToken = await jwtManage.saveToken(user);

      // Send the jwt in the response
      if (userWithToken.token) {

        // Check if user is Doctor
        //const result = (user.role === 'Doctor') ? await doctorModel.getOneByUserId(user.id) : userWithToken;
        const result = userWithToken;
        res.status(200).json({ code: 200, response: result });
      } else {
        res.status(400).json({ code: 400, response: 'Falla en la creación del token' });
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   *  Logout of a user in database
   * @param req Request
   * @param res Response
   */
  public async ctrlLogout(req: Request, res: Response): Promise<void> {
    try {
      // Logout of user
      const authModel = new AuthModel();
      const result = await authModel.logout(req.headers.authorization);

      // Validate result
      if (result) {
        res.status(200).json({ code: 200, response: 'Cerrar sesión exitosamente' });
      } else {
        res.status(400).json({ code: 400, response: 'Cierre de sesión fallido' });
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Request change password user on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlRequestChangePassword(req: Request, res: Response): Promise<void> {
    try {
      // Check email are set
      if (!req.body.email) {
        res.status(400).json({ code: 400, response: 'Correo electrónico inválido' });
        return;
      }

      // Get user from database
      const authModel = new AuthModel();
      const user = await authModel.verifyEmail(req.body.email);
      if (!user) {
        res.status(200).json({ code: 200, response: 'Si tu correo electrónico se encuentra registrado en nuestra base de datos recibirás un correo electrónico con las instrucciones para cambiar tu contraseña' });
        return;
      }

      // Generate and save token on database
      const jwtManage = new JWTService();
      user.token = await jwtManage.generateToken(user);
      const userWithToken = await jwtManage.saveToken(user);

      // Loading vars
      const env = checkEnvironment();

      // Send email to user
      const emailManage = new EmailService();
      emailManage.mailOptions(
        userWithToken.email,
        'Solicitud para cambiar la contraseña',
        `
          Hola <b>${ userWithToken.firstName }</b>,
          <br/><br/>
          Has solicitado cambiar tu contraseña a través de nuestro sitio web, para poder realizar el cambio de la misma utiliza el siguiente <a href="${ env.CLIENT_SERVER }/auth/validate/${ userWithToken.token }" target="_blank">enlace</a>
        `,
      );
      emailManage.send();

      // Send the jwt in the response
      if (userWithToken.token) {
        res.status(200).json({ code: 200, response: 'Si tu correo electrónico se encuentra registrado en nuestra base de datos recibirás un correo electrónico con las instrucciones para cambiar tu contraseña' });
      } else {
        res.status(400).json({ code: 400, response: 'Falla en la verificación del token' });
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Validate token of user on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlValidateToken(req: Request, res: Response): Promise<void> {
    try {
      // Check token are set
      if (!req.params.token) {
        res.status(400).json({ code: 400, response: 'Token inválido' });
        return;
      }

      // Validate token
      const jwtManage = new JWTService();
      const result = await jwtManage.validateToken(req.params.token);

      // Validate result
      if (result) {
        res.status(200).json({ code: 200, response: 'Token válido', token: result.token });
      } else {
        res.status(400).json({ code: 400, response: 'Token inválido' });
      }

      // validateToken
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Change password user on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlChangePassword(req: Request, res: Response): Promise<void> {
    try {
      // Check password are set
      if (!req.body.password) {
        res.status(400).json({ code: 400, response: 'Contraseña inválida' });
      }

      // Change password
      const authModel = new AuthModel();
      const result = await authModel.changePassword(req.headers.authorization, req.body.password);

      // Validate result
      if (result) {
        res.status(200).json({ code: 200, response: 'Se cambio la contraseña con éxito' });
      } else {
        res.status(400).json({ code: 400, response: 'Cambio de contraseña fallida' });
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }
}

export const authController = new AuthController();
