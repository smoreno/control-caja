export interface Administrator {
    id?: number;
    documentNumber?: number;
    firstName?: string;
    lastName?: string;
    role?: string;
    email?: string;
    password?: string;
    departamentId?: number;
}