import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';


// SERVICES
import { LocalStorageService } from './services/local-storage.service';
import { AuthService } from './services/auth.service'; //Login
import { AdministratorService } from './services/administrator.service'; //Usuarios
import { DepartamentService } from './services/departament.service'; //Departamentos
import { ProjectService } from './services/project.service'; //Empresas
import { BoxService } from './services/box.service'; //Cajas
import { MovementService } from './services/movement.service';  //Facturas o movimientos
import { TypeExpenseService } from './services/type_expense.service';  //Tipos de gastos
import { StateService } from './services/state.service';

// Interceptors
import { RequestInterceptor } from './interceptors/request.interceptor';
import { ErrorInterceptor } from './interceptors/error.interceptor';

@NgModule({
  declarations: [
  ],

  exports: [
  ],
  imports: [
      CommonModule,
  ],
  providers: [
      { provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true},
      { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
      LocalStorageService,
      AuthService,
      AdministratorService,
      DepartamentService,
      ProjectService,
      BoxService,
      MovementService,
      TypeExpenseService,
      StateService
    ],
  bootstrap: [],
})
export class CoreModule { }