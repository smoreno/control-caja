import { validate } from 'class-validator';
import { Request, Response } from 'express';


import { MovementModel } from './movement.model';

// Entities
import { Movement } from '../../core/database/entities/movement';

class MovementController {
  /**
   * Loads all movement from the database
   * @param req Request
   * @param res Response
   */
  public async ctrlList(req: Request, res: Response): Promise<void> {
    try {
      const dep = new MovementModel();
      const movement = await dep.list();
      res.status(200).json({ code: 200, response: movement });
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Load movement by id from the database
   * @param req Request
   * @param res Response
   */
  public async ctrlGetOne(req: Request, res: Response): Promise<void> {
    try {
      // Find movement
      const dep = new MovementModel();
      const movement: Movement = await dep.getOne(parseInt(req.params.id));

      // Valid the info
      if (movement) {
        res.status(200).json({ code: 200, response: movement });
      } else {
        res.status(404).json({ code: 404, response: 'Movemento no encontrado' });
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Create a movement on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlCreate(req: Request, res: Response): Promise<void> {
    try {
      const dep = new MovementModel();

      // Creating a instance of Movement
      const movement = new Movement();
      movement.numfact = req.body.numfact;
      movement.description = req.body.description;
      movement.dayfact = req.body.dayfact;
      movement.amountfact = req.body.amountfact;
      movement.attached = req.body.attached;
      movement.residue = req.body.residue;
      movement.box = req.body.boxId;
      movement.responsable = req.body.responsableId;
      movement.project = req.body.projectId;
      movement.expense = req.body.expenseId;

      // Validate data movement
      const errorsMovement = await validate(movement);
      if (errorsMovement.length > 0) {
        res.status(400).json({ code: 400, response: errorsMovement });
        return;
      }

      // Return data
      const dataMovement: Movement = await dep.saveChanges(movement);

      res.status(200).json({ code: 200, response: dataMovement });
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Update a movement on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlUpdate(req: Request, res: Response): Promise<void> {
    try {
      const dep = new MovementModel();

      // Creating a instance of Movement
      const movement = new Movement();
      movement.numfact = req.body.numfact;
      movement.description = req.body.description;
      movement.dayfact = req.body.dayfact;
      movement.amountfact = req.body.amountfact;
      movement.attached = req.body.attached;
      movement.residue = req.body.residue;
      movement.box = req.body.boxId;
      movement.responsable = req.body.responsableId;
      movement.project = req.body.projectId;
      movement.expense = req.body.expenseId;


      // Validate data Movement
      const errorsMovement = await validate(movement);
      if (errorsMovement.length > 0) {
        res.status(400).json({ code: 400, response: errorsMovement });
        return;
      }

      // Return data
      const dataMovement: Movement = await dep.saveChanges(movement);

      res.status(200).json({ code: 200, response: dataMovement });
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Remove a Movement on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlRemove(req: Request, res: Response): Promise<void> {
    try {
      // Search Doctor
      const dep = new MovementModel();
      const Movement = await dep.getOne(parseInt(req.params.id));
      if (Movement) {
        // Return data
        const data = await dep.remove(Movement.id);
        res.status(200).json({ code: 200, response: data });
      } else {
        res.status(404).json({ code: 404, response: 'Movement not found' });
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }
}

export const movementController = new MovementController();