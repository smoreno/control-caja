import * as jwt from 'jsonwebtoken';
import { getManager } from 'typeorm';
import config from '../configs/json-web-token/secret';
import { User } from '../database/entities/user';

export class JWTService {
  // Sing JWT, valid for 1 hour
  public generateToken(user: User): string {
    return jwt.sign(
      { id: user.id, email: user.email },
      config.jwtSecret,
      { expiresIn: '1h' },
    );
  }

  // Decoded JWT
  public decodeToken(token: string): any {
    return jwt.verify(token, config.jwtSecret);
  }

  // Validate the token against database
  public async searchUserByIdAndToken(userId: number, userToken: string): Promise<User> {
    //  Get repository to operations
    const userRepository = getManager().getRepository(User);

    // Return user by id and token
    return await userRepository.findOne({ where: { id: userId, token: userToken } });
  }

  // Validate the token against database
  public async validateToken(userToken: string): Promise<User> {
    //  Get repository to operations
    const userRepository = getManager().getRepository(User);

    // Return user by id and token
    return await userRepository.findOne({ where: { token: userToken } });
  }

  // Save token
  public async saveToken(user: User): Promise<User> {
    //  Get repository to operations
    const userRepository = getManager().getRepository(User);

    // Return user updated
    return await userRepository.save(user);
  }
}
