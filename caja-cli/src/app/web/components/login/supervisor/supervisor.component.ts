import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-supervisor',
  templateUrl: './supervisor.component.html',
  styleUrls: ['./supervisor.component.scss']
})
export class SupervisorComponent implements OnInit {
  operador = 'David Oliveros';
  supervisor = 'David Pineda';
  public oper = true;
  public super = true;

  constructor() { }

  ngOnInit() {

  }

}
