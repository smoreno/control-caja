import { DeleteResult, getManager } from 'typeorm';
import { Project } from '../../core/database/entities/project';


export class ProjectModel {
  public async list(): Promise<Project[]> {
    //  Get repository to operations
    const projectRepository = getManager().getRepository(Project);

    // Return the list of projects
    return await projectRepository.find({   });
  }

  public async getOne(projectId: number): Promise<Project> {
    //  Get repository to operations
    const projectRepository = getManager().getRepository(Project);

    // Return project by id
    return await projectRepository.findOne({
      relations: [ 'name', 'description', 'location'],
      where: { id: projectId},
    });
  }

  public async saveChanges(project: Project): Promise<Project> {
    return await getManager().getRepository(Project).save(project);
  }

  public async remove(projectId: number): Promise<DeleteResult> {
    return await getManager().getRepository(Project).delete(projectId);
  }
}
