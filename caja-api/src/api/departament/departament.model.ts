import { DeleteResult, getManager } from 'typeorm';
import { Departament } from '../../core/database/entities/departament';

export class DepartamentModel {
  public async list(): Promise<Departament[]> {
    //  Get repository to operations
    const DepartamentRepository = getManager().getRepository(Departament);

    // Return the list of bills
    return await DepartamentRepository.find({});
  }

  public async getOne(DepartamentId: number): Promise<Departament> {
    //  Get repository to operations
    const DepartamentRepository = getManager().getRepository(Departament);

    // Return Departament by id
    return await DepartamentRepository.findOne({
      relations: [ 'user'],
      where: { id: DepartamentId},
    });
  }

  public async saveChanges(departament: Departament): Promise<Departament> {
    return await getManager().getRepository(Departament).save(departament);
  }

  public async remove(DepartamentId: number): Promise<DeleteResult> {
    return await getManager().getRepository(Departament).delete(DepartamentId);
  }
}
