import { Router } from 'express';
import { checkJwt } from '../../core/middlewares/check-jwt';
import { stateController } from './state.controller';

class StateRoutes {
  public router: Router = Router();

  constructor() {
    this.config();
  }

  public config(): void {
    this.router.get('/', checkJwt, stateController.ctrlList);
    this.router.get('/:id([0-9]+)', checkJwt, stateController.ctrlGetOne);
    this.router.post('/', checkJwt, stateController.ctrlCreate);
    this.router.patch('/', checkJwt, stateController.ctrlUpdate);
    this.router.delete('/:id([0-9]+)', checkJwt, stateController.ctrlRemove);
  }
}

export default new StateRoutes().router;