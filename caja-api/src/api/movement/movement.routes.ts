import { Router } from 'express';
import { checkJwt } from '../../core/middlewares/check-jwt';
import { movementController } from './movement.controller';

class MovementRoutes {
  public router: Router = Router();

  constructor() {
    this.config();
  }

  public config(): void {
    this.router.get('/', checkJwt, movementController.ctrlList);
    this.router.get('/:id([0-9]+)', checkJwt, movementController.ctrlGetOne);
    this.router.post('/', checkJwt, movementController.ctrlCreate);
    this.router.patch('/', checkJwt, movementController.ctrlUpdate);
    this.router.delete('/:id([0-9]+)', checkJwt, movementController.ctrlRemove);
  }
}

export default new MovementRoutes().router;