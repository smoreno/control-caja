// import * as bcrypt from 'bcrypt';
// import { IsEmail, MaxLength } from 'class-validator';
import { Column, Entity, ManyToOne, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Departament } from './departament';
import { State } from './state';
import { User } from './user';

@Entity()
export class Box {
  @PrimaryGeneratedColumn()
  public id: number;
    // Claves Foraneas
  @OneToOne((type: any) => Departament, { onDelete: 'CASCADE' })
  public departament: Departament;

  @OneToOne((type: any) => User, { onDelete: 'CASCADE' })
  public supervisor: User;

  @ManyToOne((type: any) => State, { onDelete: 'CASCADE' })
  public state: State;
  // Fin de Claves foraneas
  @Column()
  public money: number;

  @Column()
  public observation: string;

  @Column()
  public dayopen: string;

  @Column()
  public dayclose: string;
}
