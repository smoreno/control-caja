import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

// MODULOS
import { OperadorRoutingModule } from './operador-routing.module';
import { SharedModule } from 'src/app/share/share.module';

// COMPONENTES
import { OperadorComponent } from './operador.component';

@NgModule({
  imports: [
    CommonModule,
    OperadorRoutingModule,
    SharedModule,
  ],
  declarations: [
    OperadorComponent
  ],
  providers: [],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class OperadorModule { }
