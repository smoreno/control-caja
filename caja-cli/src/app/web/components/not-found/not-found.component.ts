import { Component, OnInit } from '@angular/core';
import { IResponse } from 'src/app/share/interfaces/iresponse';
import { AuthService } from 'src/app/core/services/auth.service';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent implements OnInit {

  ngOnInit() {
  }

  constructor(
    private authService: AuthService,
    private lsService: LocalStorageService,
    private router: Router) {}

  
  logout() {
    this.authService.logout().subscribe(
      (data: IResponse) => {
        this.lsService.clearStorage();
        this.router.navigate(['/auth']);
      },
      err => {
        if (err.name === 'TokenExpiredError') {
          this.lsService.clearStorage();
          this.router.navigate(['/auth']);
        } else {
          console.log(err);
        }
      }
    );
  }
}
