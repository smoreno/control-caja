import { Router } from 'express';
import { checkJwt } from '../../core/middlewares/check-jwt';
import { boxController } from './box.controller';

class BoxRoutes {
  public router: Router = Router();

  constructor() {
    this.config();
  }

  public config(): void {
    this.router.get('/', checkJwt, boxController.ctrlList);
    this.router.get('/:id([0-9]+)', checkJwt, boxController.ctrlGetOne);
    this.router.post('/', checkJwt, boxController.ctrlCreate);
    this.router.patch('/', checkJwt, boxController.ctrlUpdate);
    this.router.delete('/:id([0-9]+)', checkJwt, boxController.ctrlRemove);
  }
}

export default new BoxRoutes().router;