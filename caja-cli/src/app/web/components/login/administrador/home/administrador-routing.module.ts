import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// COMPONENTES
import { AdministradorComponent } from './administrador.component';
import { UsuariosComponent } from '../usuarios/user-list/usuarios.component';
import { BusinessListComponent } from '../empresas/business-list/business-list.component';
import { BusinessDetailComponent } from '../empresas/business-detail/business-detail.component';
import { DepartamentosComponent } from '../departamentos/departamentos.component';
import { UserDetailComponent } from '../usuarios/user-detail/user-detail.component';
import { DetaildepComponent } from '../departamentos/detaildep/detaildep.component';

const routes: Routes = [
  { path: '', component: AdministradorComponent },
  { path: 'usuarios', component: UsuariosComponent },
  { path: 'empresas', component: BusinessListComponent },
  { path: 'departamentos', component: DepartamentosComponent },
  { path: 'userdetail', component: UserDetailComponent },
  { path: 'detailemp', component: BusinessDetailComponent },
  { path: 'detaildep', component: DetaildepComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministradorRoutingModule { }
